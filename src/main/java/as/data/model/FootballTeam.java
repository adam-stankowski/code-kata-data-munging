package as.data.model;

public class FootballTeam {
    private final String teamName;
    private final int goalsDifference;

    public FootballTeam(String teamName, int goalsDifference) {
        this.teamName = teamName;
        this.goalsDifference = goalsDifference;
    }

    public String getTeamName() {
        return teamName;
    }

    public int getGoalsDifference() {
        return goalsDifference;
    }

    @Override
    public String toString() {
        return "FootballTeam{" +
                "teamName='" + teamName + '\'' +
                ", goalsDifference=" + goalsDifference +
                '}';
    }
}
