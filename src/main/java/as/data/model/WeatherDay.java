package as.data.model;

public class WeatherDay {
    private final int dayNum;
    private final double minTemp;
    private final double maxTemp;

    public WeatherDay(int dayNum, double minTemp, double maxTemp) {
        this.dayNum = dayNum;
        this.minTemp = minTemp;
        this.maxTemp = maxTemp;
    }

    public int getDayNum() {
        return dayNum;
    }

    public double getMinTemp() {
        return minTemp;
    }

    public double getMaxTemp() {
        return maxTemp;
    }

    @Override
    public String toString() {
        return "WeatherDay{" +
                "dayNum=" + dayNum +
                ", minTemp=" + minTemp +
                ", maxTemp=" + maxTemp +
                '}';
    }
}
