package as.data.functionalinterface;

import java.io.IOException;
import java.net.URISyntaxException;

@FunctionalInterface
public interface FileDataSupplier<T>{
    T get() throws IOException, URISyntaxException;
}