package as.data;

import as.data.functionalinterface.FileDataSupplier;
import as.data.parser.impl.FileDataParserImpl;
import as.data.service.FootballTeamServiceImpl;
import as.data.service.WeatherDayServiceImpl;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Optional;

public class App {
    private static final String WEATHER_FILE = "weather.dat";
    private static final String FOOTBALL_FILE = "football.dat";

    public static void main(String[] args) {

        try {
            printData();
        } catch (IOException | URISyntaxException e) {
            e.printStackTrace();
        }

    }

    private static void printData() throws IOException, URISyntaxException {
        printDataFromFile(() -> new WeatherDayServiceImpl(WEATHER_FILE, new FileDataParserImpl()).findDayWithSmallestTempSpread());
        printDataFromFile(() -> new FootballTeamServiceImpl(FOOTBALL_FILE, new FileDataParserImpl()).findTeamWithSmallestGoalDifference());
    }

    private static <T> void printDataFromFile(FileDataSupplier<Optional<T>> supplier) throws IOException, URISyntaxException {
        supplier.get().ifPresentOrElse(App::printItem, App::noItemFound);
    }

    private static <T> void printItem(T item) {
        System.out.println(String.format("Found item is %s", item));
    }

    private static void noItemFound() {
        System.out.println("Item could not be found");
    }
}