package as.data.service;

import as.data.model.FootballTeam;
import as.data.parser.FileDataParser;
import as.data.parser.impl.LineToFootballTeamConverter;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Comparator;
import java.util.Optional;
import java.util.stream.Collectors;

public class FootballTeamServiceImpl extends FileDataExtractor<FootballTeam> implements FootballTeamService {

    public FootballTeamServiceImpl(String filename, FileDataParser fileDataParser) throws IOException, URISyntaxException {
        super(filename, fileDataParser, lines -> lines
                .skip(2)
                .map(LineToFootballTeamConverter::lineToFootballTeam)
                .flatMap(Optional::stream));
    }

    @Override
    public Optional<FootballTeam> findTeamWithSmallestGoalDifference() {
        return extractItems(footballTeams -> footballTeams.stream().collect(Collectors.maxBy(Comparator.comparing(FootballTeam::getGoalsDifference))));
    }
}
