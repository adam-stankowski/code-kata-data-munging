package as.data.service;

import as.data.model.WeatherDay;
import as.data.parser.FileDataParser;
import as.data.parser.impl.LineToWeatherDayConverter;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Comparator;
import java.util.Optional;
import java.util.function.BinaryOperator;

public class WeatherDayServiceImpl extends FileDataExtractor<WeatherDay> implements WeatherDayService{

    public WeatherDayServiceImpl(String filename, FileDataParser fileDataParser) throws IOException, URISyntaxException {
        super(filename, fileDataParser, lines -> lines
                .skip(2)
                .map(LineToWeatherDayConverter::lineToWeatherDay)
                .flatMap(Optional::stream));
    }

    @Override
    public Optional<WeatherDay> findDayWithSmallestTempSpread() {
        return extractItems(weatherDays -> weatherDays.parallelStream()
                .reduce(BinaryOperator.minBy(
                        Comparator.comparing(weatherDay -> (weatherDay.getMaxTemp() - weatherDay.getMinTemp())))));
    }
}
