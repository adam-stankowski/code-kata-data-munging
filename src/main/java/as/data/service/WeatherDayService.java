package as.data.service;

import as.data.model.WeatherDay;

import java.util.Optional;

public interface WeatherDayService {
    Optional<WeatherDay> findDayWithSmallestTempSpread();
}
