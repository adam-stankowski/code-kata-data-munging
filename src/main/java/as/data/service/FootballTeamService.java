package as.data.service;

import as.data.model.FootballTeam;

import java.util.Optional;

public interface FootballTeamService {
    Optional<FootballTeam> findTeamWithSmallestGoalDifference();
}
