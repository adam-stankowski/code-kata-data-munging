package as.data.service;

import as.data.parser.FileDataParser;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Stream;

abstract class FileDataExtractor <T>{
    private List<T> itemList;

    protected FileDataExtractor(String filename, FileDataParser fileDataParser, Function<Stream<String>, Stream<T>> lineConverter)
            throws IOException, URISyntaxException {
        Objects.requireNonNull(filename);
        itemList = fileDataParser.openFileAndParseData(filename, lineConverter);
    }

    protected Optional<T> extractItems (Function<List<T>, Optional<T>> extractorFunction) {
        return extractorFunction.apply(itemList);
    }
}
