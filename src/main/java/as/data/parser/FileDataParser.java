package as.data.parser;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Stream;

public interface FileDataParser {
   <T> List<T> openFileAndParseData(String filename, Function<Stream<String>, Stream<T>> lineConverter) throws URISyntaxException, IOException;
}
