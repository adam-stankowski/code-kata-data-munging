package as.data.parser.impl;

import as.data.parser.FileDataParser;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class FileDataParserImpl implements FileDataParser {

    private static final String BAD_FILENAME = "Filename cannot be empty or null";

    @Override
    public <T> List<T> openFileAndParseData(String filename, Function<Stream<String>, Stream<T>> lineConverter) throws URISyntaxException, IOException {
        throwIfInvalidInput(filename);
        URI fileUri = getClass().getClassLoader().getResource(filename).toURI();
        Path path = Paths.get(fileUri);
        Stream<String> lines = Files.lines(path, Charset.forName("UTF-8"));
        return lineConverter.apply(lines).collect(Collectors.toList());
    }

    private void throwIfInvalidInput(String filename) {
        if (filename == null || filename.isEmpty()) {
            throw new IllegalArgumentException(BAD_FILENAME);
        }
    }
}
