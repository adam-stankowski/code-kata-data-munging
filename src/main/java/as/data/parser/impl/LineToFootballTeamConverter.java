package as.data.parser.impl;

import as.data.model.FootballTeam;

import java.util.Optional;

public class LineToFootballTeamConverter {
    public static Optional<FootballTeam> lineToFootballTeam(String line){
        if (line == null || line.isEmpty()) {
            return Optional.empty();
        }

        String[] split = line.trim().split("-*\\s+");

        if(split.length < 9){
            return Optional.empty();
        }

        return extractValidFootballTeamData(split);

    }

    private static Optional<FootballTeam> extractValidFootballTeamData(String[] split) {
        String teamName = split[1];
        int goalsFor = Integer.parseInt(split[6]);
        int goalsAgainst = Integer.parseInt(split[8]);
        return Optional.of(new FootballTeam(teamName, Math.abs(goalsFor - goalsAgainst)));
    }
}
