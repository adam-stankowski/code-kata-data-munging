package as.data.parser.impl;

import as.data.model.WeatherDay;

import java.util.Optional;

public class LineToWeatherDayConverter {
    public static Optional<WeatherDay> lineToWeatherDay(String line) {
        if (line == null || line.isEmpty()) {
            return Optional.empty();
        }

        try {
            return parseLineToWeatherDay(line);
        } catch (NumberFormatException e) {
            return Optional.empty();
        }
    }

    private static Optional<WeatherDay> parseLineToWeatherDay(String line) {
        String[] split = line.trim().split("\\*?\\s+");
        int day = Integer.parseInt(split[0]);
        double maxTemp = Double.parseDouble(split[1]);
        double minTemp = Double.parseDouble(split[2]);
        return Optional.of(new WeatherDay(day, minTemp, maxTemp));
    }
}
