package as.data.parser;

import as.data.model.FootballTeam;
import as.data.model.WeatherDay;
import as.data.parser.impl.FileDataParserImpl;
import as.data.parser.impl.LineToFootballTeamConverter;
import as.data.parser.impl.LineToWeatherDayConverter;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;

public class FileDataParserTest {
    private static final String WEATHER_TEST_FILE = "weather_test1.dat";
    private static final String FOOTBALL_TEST_FILE = "football_test1.dat";

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    private FileDataParser fileDataParser;

    @Before
    public void setUp() {
        fileDataParser = new FileDataParserImpl();
    }

    @Test
    public void whenFilenameEmptyOrNullThenException() throws IOException, URISyntaxException {

        expectedException.expect(IllegalArgumentException.class);
        fileDataParser.openFileAndParseData(null, Function.identity());

        expectedException.expect(IllegalArgumentException.class);
        fileDataParser.openFileAndParseData("", Function.identity());
    }

    @Test
    public void whenParsingCorrectWeatherDayFileInClasspathThenParsedOk() throws IOException, URISyntaxException {

        List<WeatherDay> weatherDays = fileDataParser.openFileAndParseData(WEATHER_TEST_FILE,
                lines -> lines
                        .skip(2)
                        .map(LineToWeatherDayConverter::lineToWeatherDay)
                        .flatMap(Optional::stream));
        assertThat(weatherDays, hasSize(14));
    }

    @Test
    public void whenParsingCorrectFootballTeamFileInClasspathThenParsedOk() throws IOException, URISyntaxException {

        List<FootballTeam> footballTeams = fileDataParser.openFileAndParseData(FOOTBALL_TEST_FILE,
                lines -> lines
                        .skip(1)
                        .map(LineToFootballTeamConverter::lineToFootballTeam)
                        .flatMap(Optional::stream));
        assertThat(footballTeams, hasSize(10));
    }
}