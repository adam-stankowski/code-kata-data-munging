package as.data.parser.impl;

import as.data.model.FootballTeam;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

public class LineToFootballTeamConverterTest {

    @Before
    public void setUp() {
    }

    @Test
    public void whenEmptyStringThenOptionalEmpty() {
        Optional<FootballTeam> footballTeam = LineToFootballTeamConverter.lineToFootballTeam("");
        assertThat(footballTeam, is(Optional.empty()));
    }

    @Test
    public void whenCorrectLineThenParseCorrectWeatherData() {
        String line = "    9. Tottenham       38    14   8  16    49  -  53    50";
        Optional<FootballTeam> footballTeam = LineToFootballTeamConverter.lineToFootballTeam(line);
        assertThat(footballTeam.isPresent(), is(true));
        assertThat(footballTeam.get().getTeamName(), is("Tottenham"));
        Assert.assertEquals(4, footballTeam.get().getGoalsDifference());
    }

    @Test
    public void whenUnexpectedInputThenSkipLine(){
        String line = "   -------------------------------------------------------";
        Optional<FootballTeam> footballTeam = LineToFootballTeamConverter.lineToFootballTeam(line);
        assertThat(footballTeam.isPresent(), is(false));
    }
}