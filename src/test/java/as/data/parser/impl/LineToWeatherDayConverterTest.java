package as.data.parser.impl;

import as.data.model.WeatherDay;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;


public class LineToWeatherDayConverterTest {

    @Before
    public void setUp() {
    }

    @Test
    public void whenEmptyStringThenOptionalEmpty() {
        Optional<WeatherDay> weatherDay = LineToWeatherDayConverter.lineToWeatherDay("");
        assertThat(weatherDay, is(Optional.empty()));
    }

    @Test
    public void whenCorrectLineThenParseCorrectWeatherData() {
        String line = "  1  88    59    74          53.8       0.00 F       280  9.6 270  17  1.6  93 23 1004.5";
        Optional<WeatherDay> weatherDay = LineToWeatherDayConverter.lineToWeatherDay(line);
        assertThat(weatherDay.isPresent(), is(true));
        Assert.assertEquals(1, weatherDay.get().getDayNum());
        Assert.assertEquals(59, weatherDay.get().getMinTemp(), 0);
        Assert.assertEquals(88, weatherDay.get().getMaxTemp(), 0);
    }

    @Test
    public void whenOneValueInLinewithAsteriskThenAsteriskSkipped() {
        String line = "   9  86    32*   59       6  61.5       0.00         240  7.6 220  12  6.0  78 46 1018.6";
        Optional<WeatherDay> weatherDay = LineToWeatherDayConverter.lineToWeatherDay(line);
        assertThat(weatherDay.isPresent(), is(true));
        Assert.assertEquals(9, weatherDay.get().getDayNum());
        Assert.assertEquals(32, weatherDay.get().getMinTemp(), 0);
        Assert.assertEquals(86, weatherDay.get().getMaxTemp(), 0);
    }

    @Test
    public void whenUnexpectedInputThenSkipLine(){
        String line = "  mo  82.9  60.5  71.7    16  58.8       0.00              6.9          5.3";
        Optional<WeatherDay> weatherDay = LineToWeatherDayConverter.lineToWeatherDay(line);
        assertThat(weatherDay.isPresent(), is(false));
    }
}