package as.data.service;

import as.data.model.WeatherDay;
import as.data.parser.FileDataParser;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;

public class WeatherDayServiceTest {

    private static final String ANY_FILENAME = "Any_filename";
    private WeatherDayService weatherDayService;

    private FileDataParser fileDataParser;

    @Before
    public void setUp() {
        fileDataParser = Mockito.mock(FileDataParser.class);
    }

    @Test
    public void whenWeatherDayListEmptyThenEmpty() throws IOException, URISyntaxException {
        Mockito.when(fileDataParser.openFileAndParseData(anyString(), anyObject())).thenReturn(new ArrayList<>());
        weatherDayService = new WeatherDayServiceImpl(ANY_FILENAME, fileDataParser);
        Optional<WeatherDay> day = weatherDayService.findDayWithSmallestTempSpread();
        assertThat(day.isPresent(), is(false));
    }

    @Test
    public void whenOneDayInListThenItsReturned() throws IOException, URISyntaxException {
        List<WeatherDay> oneDayList = Arrays.asList(new WeatherDay(1, 12, 42));
        Mockito.doReturn(oneDayList).when(fileDataParser).openFileAndParseData(anyString(), anyObject());
        weatherDayService = new WeatherDayServiceImpl(ANY_FILENAME, fileDataParser);
        Optional<WeatherDay> day = weatherDayService.findDayWithSmallestTempSpread();
        assertThat(day.isPresent(), is(true));
        assertThat(day.get().getDayNum(), is(1));
        assertThat(day.get().getMinTemp(), is(12.0));
    }

    @Test
    public void whenSeveralDaysWithVariousDifferenceThenDayWithMinDifferenceSelected() throws IOException, URISyntaxException {
        List<WeatherDay> daysList = Arrays.asList(
                new WeatherDay(1, 12, 42), new WeatherDay(2, 22, 37),
                new WeatherDay(3, 17, 22), new WeatherDay(4, 22, 25));
        Mockito.doReturn(daysList).when(fileDataParser).openFileAndParseData(anyString(), anyObject());
        weatherDayService = new WeatherDayServiceImpl(ANY_FILENAME, fileDataParser);
        Optional<WeatherDay> day = weatherDayService.findDayWithSmallestTempSpread();
        assertThat(day.isPresent(), is(true));
        assertThat(day.get().getDayNum(), is(4));
        assertThat(day.get().getMinTemp(), is(22.0));
    }

    @Test
    public void whenTwoDaysTheSameDifferenceThenOneIsChosen() throws IOException, URISyntaxException {
        List<WeatherDay> daysList = Arrays.asList(
                new WeatherDay(1, 12, 42), new WeatherDay(2, 10, 40));
        Mockito.doReturn(daysList).when(fileDataParser).openFileAndParseData(anyString(), anyObject());
        weatherDayService = new WeatherDayServiceImpl(ANY_FILENAME, fileDataParser);
        Optional<WeatherDay> day = weatherDayService.findDayWithSmallestTempSpread();
        assertThat(day.isPresent(), is(true));
        assertThat(day.get().getDayNum(), is(1));
        assertThat(day.get().getMinTemp(), is(12.0));
    }
}