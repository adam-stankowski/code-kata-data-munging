package as.data.service;

import as.data.model.FootballTeam;
import as.data.parser.FileDataParser;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;

public class FootballTeamServiceTest {

    private static final String ANY_FILENAME = "Any_filename";
    private static final String TEST_TEAM_NAME = "Any_Team";
    private FootballTeamService footballTeamService;
    private FileDataParser fileDataParser;

    @Before
    public void setUp() {
        fileDataParser = Mockito.mock(FileDataParser.class);
    }

    @Test
    public void whenFootballTeamListEmptyThenEmpty() throws IOException, URISyntaxException {
        Mockito.when(fileDataParser.openFileAndParseData(anyString(), anyObject())).thenReturn(new ArrayList<>());
        footballTeamService = new FootballTeamServiceImpl(ANY_FILENAME, fileDataParser);
        Optional<FootballTeam> footballTeam = footballTeamService.findTeamWithSmallestGoalDifference();
        assertThat(footballTeam.isPresent(), is(false));
    }

    @Test
    public void whenOneDayInListThenItsReturned() throws IOException, URISyntaxException {
        List<FootballTeam> oneDayList = Arrays.asList(new FootballTeam(TEST_TEAM_NAME, 12));
        Mockito.doReturn(oneDayList).when(fileDataParser).openFileAndParseData(anyString(), anyObject());
        footballTeamService = new FootballTeamServiceImpl(ANY_FILENAME, fileDataParser);
        Optional<FootballTeam> day = footballTeamService.findTeamWithSmallestGoalDifference();
        assertThat(day.isPresent(), is(true));
        assertThat(day.get().getTeamName(), is(TEST_TEAM_NAME));
        assertThat(day.get().getGoalsDifference(), is(12));
    }

    @Test
    public void whenSeveralDaysWithVariousDifferenceThenTeamWithMaxGoalDifferenceSelected() throws IOException, URISyntaxException {
        List<FootballTeam> daysList = Arrays.asList(
                new FootballTeam("TeamOne", 12), new FootballTeam("TeamTwo", 14),
                new FootballTeam("TeamThree", 15), new FootballTeam("TeamFour", 11));
        Mockito.doReturn(daysList).when(fileDataParser).openFileAndParseData(anyString(), anyObject());
        footballTeamService = new FootballTeamServiceImpl(ANY_FILENAME, fileDataParser);
        Optional<FootballTeam> day = footballTeamService.findTeamWithSmallestGoalDifference();
        assertThat(day.isPresent(), is(true));
        assertThat(day.get().getTeamName(), is("TeamThree"));
        assertThat(day.get().getGoalsDifference(), is(15));
    }

    @Test
    public void whenTwoTeamsTheSameDifferenceThenOneIsChosen() throws IOException, URISyntaxException {
        List<FootballTeam> daysList = Arrays.asList(
                new FootballTeam("TeamOne", 7), new FootballTeam("TeamTwo", 7));
        Mockito.doReturn(daysList).when(fileDataParser).openFileAndParseData(anyString(), anyObject());
        footballTeamService = new FootballTeamServiceImpl(ANY_FILENAME, fileDataParser);
        Optional<FootballTeam> day = footballTeamService.findTeamWithSmallestGoalDifference();
        assertThat(day.isPresent(), is(true));
        assertThat(day.get().getGoalsDifference(), is(7));
    }
}